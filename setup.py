from setuptools import setup, find_packages

setup(
    name='py-apex',
    version='0.0.1',
    author='Geoff Johnson',
    author_email='geoff.johnson@coanda.ca',
    description='Apex API',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'pyzmq~=17.1.2',
    ],
    tests_require=[
        'pytest',
    ],
)

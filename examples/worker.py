"""Majordomo Protocol worker example.
Uses the mdwrk API to hide all MDP aspects
Author: Min RK <benjaminrk@gmail.com>
"""

import sys
from apex.mdp import Worker


def main():
    verbose = '-v' in sys.argv
    worker = Worker("tcp://localhost:5555", b'echo', verbose)
    reply = None
    while True:
        request = worker.recv(reply)
        if request is None:
            break  # Worker was interrupted
        reply = request  # Echo is complex... :-)


if __name__ == '__main__':
    main()

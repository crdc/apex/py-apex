# Apex Python Library

* Client and worker API
* Compiled Protobuf files from [apex/proto](https://gitlab.com/crdc/apex/apex-proto)
* Apex Module class to use as a starting point
